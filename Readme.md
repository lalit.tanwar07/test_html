This Sample HTML Project to Create Mockups.

You need php,composer,git installed on your system.

Download the porject

And start puttting you html files in 'resources/your_project' folder

Browser URL should be http://localhost/html_mockups/web/your_project

Inside your_project folder you can have your htmls files and css at any level inside this, you starting html file should be

'resources/your_project/index.html'

To include on html file into other use 'include' tag

```
<include src="relative_path_to_other_html" data={} />
```

CSS files need to be listed in 'resources/your_project/style.scss'


If you need data in html files, that can be put into 'resources/your_project/data.json' file